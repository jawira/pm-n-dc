Step 2 - File permissions
=========================

All files and directories should be writable. 

![Step 2](../resources/pictures/step-2.png)

Click on `next` button.

**[← Step 1](./step-1.md) | [Step 3 →](./step-3.md)**
