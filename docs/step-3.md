Step 3 - License
================

Accept ProcessMaker license. 

![Step 3](../resources/pictures/step-3.png)

1. Check `I agree` checkbox.
2. Click on `next` button.

**[← Step 2](./step-2.md) | [Step 4 →](./step-4.md)**
