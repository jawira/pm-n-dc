Step 8 - Login
==============

Login to ProcessMaker.

![Step 8](../resources/pictures/step-8.png)

1. Fill `username` and `password`.
2. Click on `Login` button.
3. Your can start working with ProcessMaker.

**[← Step 7](./step-7.md)**
