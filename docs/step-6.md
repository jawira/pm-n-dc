Step 6 - Successful installation
================================

ProcessMaker was successfully installed.

![Step 6](../resources/pictures/step-6.png)

Click on `OK` button.

**[← Step 5](./step-5.md) | [Step 7 →](./step-7.md)**
