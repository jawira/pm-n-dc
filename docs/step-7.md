Step 7 - Welcome message
========================

Read the welcome message.

![Step 7](../resources/pictures/step-7.png)

Close window, click on `x`.

**[← Step 6](./step-6.md) | [Step 8 →](./step-8.md)**
